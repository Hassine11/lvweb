
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
window.Vue = require('vue');
window.VueAxios = require('vue-axios');
window.VueRouter = require('vue-router').default;
import AppLayout from './components/App.vue';
Vue.use(require('vue-moment'));
import store from './store/index'



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 const Article = Vue.component('articles', require('./components/article/Articles.vue'));
 const ViewArticle =  Vue.component('ViewArticle', require('./components/article/showArticle.vue'));
 const Admin = Vue.component('admin',require('./components/article/admin.vue'));
 const AddArticle = Vue.component('addArticle',require('./components/article/addArticle.vue'));
 const EditArticle = Vue.component('EditArticle',require('./components/article/editArticle.vue'));
 const Follow = Vue.component('follow',require('./components/follow/follow.vue'));
 const Profile = Vue.component('profile',require('./components/profile/profile.vue'));
 Vue.use(VueRouter,VueAxios);
const routes = [

    {
        name:'articles',
        path : '/articles',
        component : Article
    },
    {
        name:'admin',
        path : '/article/admin',
        component : Admin
    },
    {
     name : 'EditArticle',
     path : '/article/admin/edit/:id',
     component : EditArticle
    },
    {
     name: 'addArticle',
     path: '/article/admin/add',
     component : AddArticle
    },
    {
        name:'follow',
        path :'/follow/user',
        component : Follow
    },
    {
        name:'ViewArticle',
        path :'/article/:id',
        component : ViewArticle
    },
    {
        name:'profile',
        path :'/profile/:user',
        component : Profile
    },
]
const router = new VueRouter({ mode: 'hash', routes: routes});
window.onload = function() {
new Vue(
    Vue.util.extend(
     {router} ,
    AppLayout,
    store
    )
   ).$mount('#app');

    }
