let mutations = {
    SET_ARTICLES(state, articles) {
        state.articles = articles
    }
}

export default mutations
