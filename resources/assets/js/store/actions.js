let actions = {
    SEARCH_ARTICLES({commit}, query) {
        let params = {
            query
        };
        axios.get(`/api/articles/search`, {params})
            .then(res => {
                if (res.data === 'ok')
                    console.log('request sent successfully')

            }).catch(err => {
            console.log(err)
        })
    },

    GET_ARTICLES({commit}) {
        axios.get('api/articles')
        .then(res => {
            commit('SET_ARTICLES',res.data)
        }).catch(err => {
            console.log(err)
        })
    }

}

export default actions
