<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/* ***** Article WebServices ****** */
Route::get('api/articles','ArticlesController@index');
Route::get('api/article/{id}','ArticlesController@show');
Route::post('api/article','ArticlesController@store' );
Route::put('api/article/{id}','ArticlesController@update');
Route::delete('api/article/{id}','ArticlesController@delete');
Route::get('api/article/user','ArticlesController@profile');
Route::post('api/image','ArticlesController@upload' );
Route::get('api/articles/followers','ArticlesController@articleByFollowers' );
Route::get('api/articles/search','ArticlesController@search' );
Route::get('api/articles/role','ArticlesController@CRUD');
Route::get('api/articles/user','ArticlesController@articleByUsers');


/* ***** Comment WebServices ****** */
Route::post('api/comment','CommentsController@store');
Route::delete('api/comment/{id}','CommentsController@delete');
Route::get('api/comment/user','CommentsController@profile');
Route::get('api/comment/article/{id}','CommentsController@show');
/* ***** Follow WebServices ****** */
Route::get('api/follow/user','FollowController@index');
Route::post('api/follow/user','FollowController@store');
Route::delete('api/follow/follows/{id}','FollowController@unfollow');
Route::get('api/followers','FollowController@test');






