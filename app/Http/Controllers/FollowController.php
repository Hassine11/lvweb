<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Follow;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FollowController extends Controller
{

public function index()
{
   return  DB::table('users')->where('id','not like',Auth::id())->get();

}

public function fetch() {
    return Follow::all();
}
public function test() {
    $followed = DB::table('follows')->select('follow_id')->where('user_id','like',Auth::id())->get();
  return  array_pluck($followed,'follow_id');
}
public function store(Request $request) {

    return Follow::updateOrCreate($request->all());
}

public function unfollow(Request $request , $id) {
    return DB::table('follows')->where('follow_id','like',$id)->delete();
}

}
