<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article ;
use App\Follow ;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ArticlesController extends Controller
{
    public function index () {

        return Article::all();
   }

    public function CRUD()
   {
     $admin = User::where('ROLE','like','ROLE_ADMIN')->pluck('id')->toArray();
     if(in_array(Auth::id(),$admin)) {
        return Article::all();
     } else {
        return  DB::table('articles')->where('user_id','like',Auth::id())->get();
     }
   }

public function articleByFollowers () {

        $follows = DB::table('follows')->select('follow_id')->where('user_id','like',Auth::id())->get();
        $follow = array_pluck($follows,'follow_id');
        array_push($follow,Auth::id());
         return  DB::table('articles')->whereIn('user_id',$follow)->get();
    }

    public function articleByUsers () {

         $articles =  DB::table('articles')->where('user_id','like',Auth::id())->get()->count();
         $comments =  DB::table('comments')->where('user_id','like',Auth::id())->get()->count();
         return response()->json(['articles' => $articles , 'comments'=>$comments]);
    }



public function show($id) {
    return Article::findOrFail($id);
}
public function store(Request $request) {

    return Article::create($request->all());
}
 public function upload(Request $request)
{
    $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images'), $imageName);

    	return response()->json(['success'=>'You have successfully upload image.']);

}
public function update(Request $request,$id) {
     $article = Article::findOrFail($id);
     $article->update($request->all()) ;
     return $article;
}
public function delete(Request $request ,$id) {
     $article = Article::findOrFail($id);
     $article->delete($id);
}

public function search(Request $request)
{
    $follows = DB::table('follows')->select('follow_id')->where('user_id','like',Auth::id())->get();
    $follow = array_pluck($follows,'follow_id');
    $resp =  DB::table('articles')->whereIn('user_id',$follow)->get()->toArray();
    $query = $request->query('query');
    $search = DB::table('articles')->where('title', 'like', '%' . $query . '%')->get();
    $result = [];
      foreach ($search as $s ) {
          if(in_array($s,$resp)){
              array_push($result,$s);
          }
      } if(empty($result)) {
          return response()->json(['articles'=> $result ,'Message' => ' Articles Not Found']);
      } else {
        return response()->json(['articles'=> $result ,'Message' => '']);
      }

}}




